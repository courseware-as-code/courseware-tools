FROM ruby:2.7.2

WORKDIR /courseware-tools

COPY . /courseware-tools

RUN bundle install

CMD ["ruby", "lib/courseware/tools.rb"]